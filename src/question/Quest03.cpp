#include <stdio.h>
#include <vector>
#include <utility>
#include <stack>

using namespace std;
const int nRowMax = 10;
const int nColMax = 10;
int matrix[nRowMax][nColMax];

bool IsG2LessG1(int G1, int G2){
    stack<int> stRow;
    stRow.push(G1);

    while(!stRow.empty()){
        int val = stRow.top();
        stRow.pop();

        for(int i = 0; i < nRowMax; ++i){
            if(matrix[val][i] != 0){

                if(i == G2)
                    return true;
                stRow.push(i);
            }
        }
    }

    return false;
}

int main(int argc, char *argv[])
{
    int g1 = 2, g2 = 3;
    vector<pair<int, int>> vecData = {
        {1,2},{2,3},{1,3},{4,3}
    };


    for(int i = 0; i < nRowMax; ++i){
        for(int j = 0; j < nColMax; ++j){
            matrix[i][j] = 0;
        }
    }

    for(auto iter = vecData.begin(); iter != vecData.end(); ++iter){
        int x = iter->first;
        int y = iter->second;
        //x>y

        matrix[x][y] = 1;
    }

    bool bFlag = false;

    bFlag = IsG2LessG1(g1, g2);
    printf("%d, g1: %d, g2: %d\n", bFlag == true ? 1 : -1, g1, g2);



    bFlag = IsG2LessG1(g2, g1);
    printf("%d, g1: %d, g2: %d\n", bFlag == true ? 1 : -1, g1, g2);

    return 0;
}
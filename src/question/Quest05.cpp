/**
 * 乘积最大子序列,时间O(n^2/2),空间(n^2/2)
 */

#include <iostream>
#include <array>
#include <vector>
#include <limits>

int main(int argc, char const *argv[])
{
    const int num = 8;
    std::array<int, num> nArr = {9, 7, -1, 6, 10, 3, -6, -4};

    for(auto var : nArr)
    {
        std::cout << var << std::endl;
    }

    int matrix[num][num];
    for(int i = 0; i < num; i++)
    {
        matrix[i][i] = nArr[i];
    }
    
    int sum = std::numeric_limits<int>::lowest();
    int row, col;
    for(int i = 0; i < num; i++)
    {
        for(int j = i + 1; j < num; j++)
        {
            if((matrix[i][j] = matrix[i][j-1] * nArr[j]) > sum){
                sum = matrix[i][j];
                row = i;
                col = j;
            }
        }
    }

    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            std::cout << matrix[i][j]<<",";
        }
        std::cout << std::endl;
    }
    
    std::cout << row << ", " << col << ":" << sum << std::endl;
    return 0;
}

#include <stdio.h>
#include <iostream>
#include <utility>
#include <vector>
using namespace std;
typedef pair<bool, pair<int, int>>  result_type;
typedef vector<vector<int>> matrix_type;
#define NULL_RESULT {false, {-1,-1}}


result_type IsExist(const matrix_type &arr, int value){

    int row = arr.size();
    int col = 0;
    if(row > 0){
        col = arr[0].size();
        for(auto iter = arr.begin(); iter != arr.end(); ++iter){
            if(iter->size() < col)
                col = iter->size();
        }
    }else{
        return NULL_RESULT;
    }
        
    int i = 0, j = col - 1;
    while(i != row && j != -1){
        if(arr[i][j] == value){
            return {true, {i, j}};
        }else if(arr[i][j] > value){
            --j;
        }else{
            ++i;
        }
    }

    return NULL_RESULT;
}

int main(int argc, char const *argv[])
{
    
    // int matrix[4][4] = {
    //     {1, 2, 8, 9},
    //     {2, 4, 9, 12},
    //     {4, 7, 10, 13},
    //     {6, 8, 11, 15}
    // };

    // int row = sizeof(matrix) / sizeof(int);
    // int col = sizeof(matrix[0]) / sizeof(int);

    matrix_type matrix = {
        {1, 2, 8, 9},
        {2, 4, 9, 12},
        {4, 7, 10, 13},
        {6, 8, 11, 15}
    };

    int row = matrix.size();
    int col = matrix[0].size();

    int nValue = 5;

    cin >> nValue;
    result_type result = IsExist(matrix, nValue);

    if(result.first)
        cout << "存在! " << result.second.first << result.second.second << endl;
    else
        cout << "不存在!" << endl;
    
    return 0;
}
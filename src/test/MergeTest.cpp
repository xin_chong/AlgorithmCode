#include <stdio.h>
#include "general/MergeSort.h"

int main(int argc, char const *argv[]) {

    int leftArr[] = {-1,1,22,32,55,4,6,8,11};
    Merge(leftArr, 0, 4, sizeof(leftArr) / sizeof(int) - 1);

    for(auto elem : leftArr){
        printf("%d, ", elem);
    }
    
    printf("\n");

    int testArr[] = {3,14,1,3,5,1,-3,0,33,12};
    MergeSort(testArr, 0, sizeof(testArr) / sizeof(int) - 1);

    for(auto elem : testArr){
            printf("%d, ", elem);
    }

    return 0;
}
#include "general/BinaryTree.h"
#include <stdio.h>
#include <vector>
#include <ctime>
#include <cstdlib>


int main(int, char **){
    std::srand(std::time(0));
    SearchTree<int> sTree;

    std::vector<int> vecTestData;

    for(int i = 0; i < 20; ++i){
        vecTestData.push_back(std::rand() * 10000 % 1000);
        printf("%d,", vecTestData[i]);
    }
    printf("\n");
    
    for(int i = 0; i < 20; ++i){
        sTree.Add(&(vecTestData.data()[i]));
    }

    for(int i = 0; i < 21; ++i){
        auto pValue = sTree.Del(vecTestData[i]);
        if(pValue)
            ++(*(pValue->pElem));
    }

   for(int i = 0; i < 20; ++i){
        printf("%d,", vecTestData[i]);
    }
    printf("\n");

    return 0;
}
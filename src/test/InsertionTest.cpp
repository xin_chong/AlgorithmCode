#include <stdio.h>
#include "general/Insertion.h"

int main(int argc, char const *argv[]) {
    int elements[] = {5, 2, 4, 6, 1, 3};

    Insertion<int>(elements, sizeof(elements) / sizeof(int));

    for(auto elem : elements){
        printf("%d, ", elem);
    }
    printf("\n");

    int elements_r[] = {5, 2, 4, 6, 1, 3};

    InsertionRecursion(elements_r,sizeof(elements_r) / sizeof(int) - 1);

    for(auto elem : elements_r){
        printf("%d, ", elem);
    }
    printf("\n");
    
    return 0;
}
#include <iostream>
#include <functional>
#include <cassert>
struct DemoStruct{
    int num : 1;
    int count : 31;

    DemoStruct():num(1), count(1){

    }

    DemoStruct(int num_, int count_):num(num_), count(count_){

    }

    DemoStruct& operator =(const DemoStruct& other){
        this->num = other.num;
        this->count = other.count;
        return *this;
    }
};

struct DemoStructHelper{

    template<size_t N>
    DemoStructHelper(DemoStruct(*rArr)[N]): nSize(N){
        pArr = (*rArr);
    }

    size_t nSize;
    DemoStruct* pArr;

    DemoStructHelper& operator=(DemoStructHelper&& other){
        if(this == &other){
            return *this;
        }
        
        assert(pArr);
        for(int i = 0; i < nSize; ++i){
            assert(other.pArr);
            pArr[i] = other.pArr[i];
        }

        return *this;
    }
    template<size_t N>
    DemoStructHelper& operator=(const DemoStruct(*rArr)[N]){
        assert(N == nSize);

        return *this = DemoStructHelper(rArr);
    }
};



DemoStruct demoArr[3];



DemoStruct demoArr1[3] = {
    {0, 1},
    {0, 1},
    {0, 1}
};



int main(int, char **){
    DemoStruct d1;
    DemoStructHelper a(&demoArr);
    a = DemoStructHelper(&demoArr1);
    printf("%d, %d\n", d1.num, d1.count);

    // d1.num = 0;
    std::cout << std::hex;
    std::cout << (((d1.num & 0x80000000)) >> 31) << std::endl;
    
    int x = -1 >> 31;
    printf("%d, \n", x);

    char cTmp = 0x01 << 7;

    std::cout << (int)cTmp << std::endl;
    return 0;
}
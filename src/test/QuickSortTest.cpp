#include <stdio.h>
#include "general/QuickSort.h"

int main(int argc, char const *argv[]) {
    
    int nElements[] = {5, 3, 17, 10, 84, 19, 6, 22, 9 };
    int nLastIndex = sizeof(nElements) / sizeof(int) - 1;

    QuickSort<int, std::greater_equal<int>>(nElements, 0, nLastIndex);

    for(auto elem : nElements){
        printf("%d,", elem);
    }
    printf("\n");
    return 0;
}